## [1.0.3](https://gitlab.com/natural-solutions/portal/compare/v1.0.2...v1.0.3) (2020-04-24)


### Bug Fixes

* doc ([b49775c](https://gitlab.com/natural-solutions/portal/commit/b49775c33d4de4acdb9c9e5b57d3f70a41500e9c))

## [1.0.2](https://gitlab.com/natural-solutions/portal/compare/v1.0.1...v1.0.2) (2020-04-24)


### Bug Fixes

* gitlab-ci ([aeaabb8](https://gitlab.com/natural-solutions/portal/commit/aeaabb8fff611af5da31d2577e73ab21a5eb96fe))

## [1.0.1](https://gitlab.com/natural-solutions/portal/compare/v1.0.0...v1.0.1) (2020-04-24)


### Bug Fixes

* gitlab-ci ([ac64028](https://gitlab.com/natural-solutions/portal/commit/ac64028e5f45ab44cc97e5cf77703805d7924666))
* gitlab-ci ([5b92580](https://gitlab.com/natural-solutions/portal/commit/5b92580ccc57f5cd868945ef58b6d985c942642b))

# 1.0.0 (2020-04-24)


### Features

* cambio algo ([15f2f64](https://gitlab.com/natural-solutions/portal/commit/15f2f64285dd71df7abf7df3d21876b5e4e061d0))
* dockerfile ([fbb2601](https://gitlab.com/natural-solutions/portal/commit/fbb2601736fdeb10064e6a178362fb38f292daea))
